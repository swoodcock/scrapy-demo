import scrapy

from typing import Iterator
from scrapy.http import Request, Response


class MessagesSpider(scrapy.Spider):
    """
    Definition of the MessageSpider.
    """

    name = "messages"
    url_scheme = "https"
    url_root = "www.bwcon.de"
    allowed_domains = [url_root]
    page_num = 0
    url = (
        f"{url_scheme}://{url_root}/aus-dem-netzwerk/meldungen"
        "?pi=3235&tx_bwconlist_bwcon%5Baction%5D=loadMoreEvents"
        "&tx_bwconlist_bwcon%5Bcontroller%5D=BwconList&type=932155"
        "&cHash=4ad0241da9d6e4b5b37c2eb408b694a1&no_cache=1"
    )
    post_data = {
        "tx_bwconlist_bwcon[__referrer][@extension]": "BwconList",
        "tx_bwconlist_bwcon[__referrer][@vendor]": "SchommerMedia",
        "tx_bwconlist_bwcon[__referrer][@controller]": "BwconList",
        "tx_bwconlist_bwcon[__referrer][@action]": "list",
        "tx_bwconlist_bwcon[__referrer][arguments]": (
            "YTowOnt97325a919d3e793c2f79a9c8507b7bd31660a6094"
        ),
        "tx_bwconlist_bwcon[__referrer][@request]": (
            'a:4:{s:10:"@extension";s:9:"BwconList";s:11:"@controller";s:9:'
            '"BwconList";s:7:"@action";s:4:"list";s:7:"@vendor";s:13:'
            '"SchommerMedia";}e829a25e2da01682dce23ce2037409740fd8b2e9'
        ),
        "tx_bwconlist_bwcon[__trustedProperties]": (
            'a:2:{s:12:"clickCounter";i:1;s:9:"recordUid";i:1;}'
            "ce13c4552f4462265822d5ccb2cc929313165a94"
        ),
        "tx_bwconlist_bwcon[clickCounter]": str(page_num),
        "tx_bwconlist_bwcon[recordUid]": "268,200,160",
    }
    # Extracted from chrome dev-tools request header
    headers = {
        "Accept": "*/*",
        "Accept-Encoding": "gzip, deflate, br",
        "Accept-Language": "en-GB,en;q=0.5",
        "Connection": "keep-alive",
        "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
        "Host": "www.bwcon.de",
        "Origin": "https://www.bwcon.de",
        "Referer": "https://www.bwcon.de/aus-dem-netzwerk/meldungen",
        "User-Agent": (
            "Mozilla/5.0 (Windows NT 10.0; Win64; "
            "x64; rv:100.0) Gecko/20100101 Firefox/100.0"
        ),
        "X-Requested-With": "XMLHttpRequest",
    }

    def start_requests(self):
        """
        Get the first page of messages.
        """

        yield scrapy.FormRequest(
            url=self.url,
            headers=self.headers,
            formdata=self.post_data,
            callback=self.parse,
        )

    def parse(self, response: Response) -> Iterator[Request]:
        """
        Parse the message posts and extract detail page urls.
        """

        message_urls = response.xpath("//a[@class='eventheading']/@href").getall()

        for url in message_urls:
            yield Request(
                url=f"{self.url_scheme}://{self.url_root}{url}",
                headers=self.headers,
                callback=self.parse_message,
            )

        if message_urls:
            self.page_num += 1
            self.post_data["tx_bwconlist_bwcon[clickCounter]"] = str(self.page_num)
            yield scrapy.FormRequest(
                url=self.url,
                headers=self.headers,
                formdata=self.post_data,
                callback=self.parse,
            )

    def parse_message(self, response: Response) -> Iterator[dict]:
        """
        Extract data from the message detail page.
        """

        yield {
            "url": response.url,
            "title": response.xpath(
                "//div[@class='bwc-meldungen-detail']" "//h3//text()"
            ).get(),
            "publication_date": response.xpath(
                "//div[@class='bwc-meldungen-detail']//div[@class='date']//text()"
            ).get(),
            "description": response.xpath(
                "//div[@class='bwc-meldungen-detail']//h5//text()"
            ).get(),
            "body": response.xpath(
                "//div[@class='bwc-meldungen-detail']//article//text()"
            ).extract(),
        }
