from scrapy import Field, Item


class MessageItem(Item):
    """
    Definition of the MessageItem.
    """

    url = Field()
    title = Field()
    publication_date = Field()
    description = Field()
    body = Field()
