import sys
import logging

from typing import Union, NoReturn
from pathlib import Path
from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings
from bwcon.bwcon.spiders.messages import MessagesSpider


log = logging.getLogger(__name__)


def _debugger_is_active() -> bool:
    """Check to see if running in debug mode.

    Returns:
        bool: if a debug trace is present or not.
    """

    gettrace = getattr(sys, "gettrace", lambda: None)
    return gettrace() is not None


def load_dotenv_if_in_debug_mode(env_file: Union[Path, str]) -> NoReturn:
    """Load secret .env variables from repo for debugging.

    Args:
        env_file (Union[Path, str]): String or Path like object pointer to
            secret dot env file to read.
    """

    try:
        from dotenv import load_dotenv
    except ImportError as e:
        log.error(
            """
            Unable to import dotenv.
            Note: The logger should be invoked after reading the dotenv file
            so that the debug level is by the environment.
            """
        )
        log.error(e)
        raise ImportError(
            """
            Unable to import dotenv, is python-dotenv installed?
            Try installing this package using pip install envidat[dotenv].
            """
        )

    if _debugger_is_active():
        secret_env = Path(env_file)
        if not secret_env.is_file():
            log.error(
                """
                Attempted to import dotenv, but the file does not exist.
                Note: The logger should be invoked after reading the dotenv file
                so that the debug level is by the environment.
                """
            )
            raise FileNotFoundError(
                f"Attempted to import dotenv, but the file does not exist: {env_file}"
            )
        else:
            load_dotenv(secret_env)


def main():
    """Main script logic."""

    load_dotenv_if_in_debug_mode(env_file=".env")

    settings = get_project_settings()

    process = CrawlerProcess(settings)
    process.crawl(MessagesSpider)
    process.start()


if __name__ == "__main__":
    main()
